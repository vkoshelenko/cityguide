CREATE TABLE city_guide.city_guide_category (
	id bigint NOT NULL auto_increment,
	name varchar(255) NOT NULL,
	description varchar(1000),
	parentId bigint,
	PRIMARY KEY (id)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE city_guide.city_guide_ad (
	id bigint(19) NOT NULL auto_increment,
	address varchar(255),
	cep varchar(255),
	city varchar(255),
	country varchar(255),
	description longtext,
	name varchar(255) NOT NULL,
	phone varchar(255),
	phone2 varchar(255),
	photo1 longblob,
	photo2 longblob,
	photo3 longblob,
	photo4 longblob,
	state varchar(255),
	url varchar(255),
	categoryId bigint(19) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY(categoryId) references city_guide.city_guide_category(id)
		on delete cascade
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;