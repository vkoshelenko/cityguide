(function ($) {

    var CategoriesManager = function(config) {

        this.buttonClass = config.buttonClass;

        global = this;

        this._init();
    };

    $.extend(true, CategoriesManager, {

        prototype: {

            /*=== Initialization ===*/
            _init: function(){
                this._bindListeners();
            },

            /*=== Binding listeners ===*/
            _bindListeners: function(event){
                $("." + this.buttonClass).toggle(this._hideHandler, this._displayHandler);
            },

            /*=== Handlers ===*/
            _hideHandler: function(event){
                var element = $(this);
                element.siblings("div").hide();
                element.text(" + ");
            },

            _displayHandler: function(event){
                var element = $(this);
                element.siblings("div").show();
                element.text(" - ");
            }
        }
    });

    window.CategoriesManager = CategoriesManager;

})(jQuery);