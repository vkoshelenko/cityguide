(function ($) {

    var CityGuide = function(config){

        this.deleteCategoryUrl = config.deleteCategoryUrl;

        global = this;

        this._init();
    };

    $.extend(true, CityGuide, {

        prototype: {

            _init: function(){
                this._bindListeners();
            },

            /*=== LISTENERS ===*/
            _bindListeners: function(){
                this._addCategoriesLinkClickListener();
                this._addAdsLinkClickListener();
                this._deleteCategoryListener();
            },

            _addCategoriesLinkClickListener: function(){
                $("#categoriesLink").on("click", function(){
                    $("#categoriesSection").show();
                    $("#adsSection").hide();
                });
            },

            _addAdsLinkClickListener: function(){
                $("#adsLink").on("click", function(){
                    $("#adsSection").show();
                    $("#categoriesSection").hide();
                });
            },

            _deleteCategoryListener: function(){
                $(".deleteCategory").on("click", this._deleteCategoryHandler);
            },

            /*=== HANDLERS ===*/
            _deleteCategoryHandler: function(event){

                var scope = this;

                var categoryId = $(this).attr("data-id");

                alert("categoryId : " + categoryId);

                var url = global.deleteCategoryUrl;

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryId : categoryId},
                    success: function(result) {
                        alert("success: " + JSON.stringify(result));
                        //remove tr
                        $(scope).parent().parent().remove();
                    },
                    error: function(result){
                        //handle error
                        alert("error: " + JSON.stringify(result));
                    }
                });
            }
        }
    });

    window.CityGuide = CityGuide;

})(jQuery);