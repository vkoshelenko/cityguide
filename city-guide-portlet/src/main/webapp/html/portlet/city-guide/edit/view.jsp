<%@ include file="init.jsp" %>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${success ne null}">
                <div class="portlet-msg-success">
                    ${success}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${error ne null}">
                    <div class="portlet-msg-error">
                        ${error}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <form action="${savePreferences}" method="POST" class="addForm">
            <div class="formRow">
                <div class="left">
                    Default city:
                </div>
                <div class="right">
                    <c:if test="${errorMap['defaultCity'] ne null}">
                        <label class="error">${errorMap['defaultCity']}</label>
                    </c:if>
                    <input type="text" name="defaultCity" id="defaultCity" value="${preferences.defaultCity}" />
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Default state:
                </div>
                <div class="right">
                    <c:if test="${errorMap['defaultState'] ne null}">
                        <label class="error">${errorMap['defaultState']}</label>
                    </c:if>
                    <input type="text" name="defaultState" id="defaultState" value="${preferences.defaultState}" />
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Default country:
                </div>
                <div class="right">
                    <c:if test="${errorMap['defaultCountry'] ne null}">
                        <label class="error">${errorMap['defaultCountry']}</label>
                    </c:if>
                    <input type="text" name="defaultCountry" id="defaultCountry" value="${preferences.defaultCountry}" />
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Max images count:
                </div>
                <div class="right">
                    <c:if test="${errorMap['maxImages'] ne null}">
                        <label class="error">${errorMap['maxImages']}</label>
                    </c:if>
                    <input type="text" name="maxImages" id="maxImages" value="${preferences.maxImages}" />
                </div>
            </div>
            <div class="navigation">
                <input type="button" value="Back" onclick="history.back();" />
                <input type="submit" value="Save Preferences" />
            </div>
        </form>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>