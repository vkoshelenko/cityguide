<%@ include file="init-edit.jsp" %>
<style type="text/css">
* {
    margin: 0;
    padding: 0;
}

#portletWrapper {
    display: block;
    background-color: white;
    border: 1px solid gray;
    border-radius: 10px;
}

#portletHeader {
    margin: 10px;
    color: #333;
    text-align: center;
}

#portletFooter {
    margin: 10px;
    color: #333;
    text-align: center;
    background-color: #eee;
}

#portletContent {
    display: block;
    margin: 10px;
    text-align: left;
}

#portletContent #menuSection {
    background-color: #eee;
    color: navy;
}

#portletContent #menuSection ul {
    display: block;
    list-style-type: none;
}
#portletContent #menuSection ul li {
    list-style-type: none;
    display: inline;
}

#portletContent #menuSection ul li a {
    text-transform: uppercase;
    text-decoration: none;
    font-weight: bold;
    color: #333;
    background-color: #eee;
    margin: 10px
}

#portletContent #menuSection ul li:hover a {
    text-transform: uppercase;
    text-decoration: none;
    font-weight: bold;
    color: #eee;
    background-color: #333;
}

#portletContent .innerContent {
    display: block;
    color: #555;
    overflow: hidden;
    clear: both;
    padding: 10px;
}

#portletContent .innerContent #innerHeader {
    font-size: 12pt;
}

#portletContent .innerContent .navigation {
    margin: 10px;
    background-color: #ddd;
    border-radius: 5px;
    color: #222;
    padding: 5px;
    display: block;
    overflow: hidden;
}

#portletContent .innerContent .navigation input[type="button"],
#portletContent .innerContent .navigation input[type="submit"] {
    background: none repeat scroll 0 0 transparent;
    background-color: #eee;
    border: 2px solid #555;
    border-radius: 8px;
    color: #333;
    font-size: 15px;
    font-weight: normal;
    margin: 5px;
    padding: 2px 20px;
}

#portletContent .innerContent .navigation input[type="button"]:hover,
#portletContent .innerContent .navigation input[type="submit"]:hover {
    background: none repeat scroll 0 0 transparent;
    background-color: #222;
    border: 2px solid #555555;
    border-radius: 8px;
    color: #eee;
    font-size: 15px;
    font-weight: normal;
    margin: 5px;
    padding: 2px 20px;
}

#portletContent .innerContent .navigation a {
    font-size: 15px;
    text-decoration: none;
    color: #333;
    margin: 5px;
    border: 2px solid #555;
    border-radius: 8px;
    background-color: #eee;
    padding: 2px 20px;
}

#portletContent .innerContent .navigation a:hover {
    font-size: 15px;
    text-decoration: none;
    color: #eee;
    background-color: #222;
}

#portletContent .innerContent .items {
    color: #444;
}

#portletContent .innerContent .items .itemButton {
    display: inline;
    border: 2px solid black;
    color: white;
    padding: 5px;
    background: #888;
    width: 15px;
    height: 15px;
}

#portletContent .innerContent .items .itemButtonInActive {
    display: inline;
    border: 2px solid black;
    color: white;
    padding: 5px;
    background: #bfbfbf;
    width: 15px;
    height: 15px;
}

#portletContent .innerContent .items a {
    text-decoration: none;
    color: #333;
}

#portletContent .innerContent .items a:hover {
    text-decoration: none;
    color: #eee;
    background-color: #222;
}

#portletContent .innerContent .items .item {
    font-size: 14px;
    margin: 15px;
}

#portletContent .innerContent .items .subItem {
    font-size: 14px;
    margin: 15px 15px 15px 40px;
}

#portletContent .innerContent .items .subSubItem {
    font-size: 14px;
    margin: 15px 15px 15px 65px;
}

#portletContent .innerContent .items table {
    margin: 5px;
}

#portletContent .innerContent .items table tr th {
    background-color: #555;
    color: #eee;
    text-align: center;
    border: 1px solid #ccc;
}

#portletContent .innerContent .items table tr th h3 {
    margin: 0;
}

#portletContent .innerContent .items table tr td {
    padding: 2px 15px;
    font-size: 10pt;
    text-align: left;
    border: 1px solid #ccc;
}

#portletContent .innerContent .items table tr td a {
    font-weight: bold;
}

.addForm {
    display: block;
    overflow: hidden;
    clear: both;
}

.formRow {
    display: block;
    overflow: hidden;
    clear: both;
    margin: 10px;
}

.formRow .left {
    display: inline;
    float: left;
    width: 170px;
}

.formRow .right {
    display: inline;
    float: left;
}

.formRow .error {
    display: block;
    color: red;
}

.formRow .right input[type="text"] {
    width: 220px;
}

.formRow .right input[type="file"] {
    width: 220px;
}

.formRow .right select {
    width: 220px;
}

.formRow .right textarea {
    width: 220px;
}

.photoBox {
    display: block;
    overflow: hidden;
}

#addPhoto {
    background: none repeat scroll 0 0 #DDDDDD;
    border: 1px solid black;
    border-radius: 10px 10px 10px 10px;
    color: #444444;
    display: inline-block;
    float: left;
    margin: 0 0 10px 300px;
    padding: 4px;
    text-align: center;
    width: 100px;
}

#photoSection {
    max-width: 500px;
}

#photoSectionTitle {
    font-weight: bold;
    margin: 5px;
}

.photoWrapper {
    width: 200px;
    height: 200px;
    border: 2px solid gray;
    display: inline-block;
    margin: 5px;
}

.deleteImage {
    background-color: #FBDAC9;
    border: 1px solid #7A0000;
    border-radius: 8px 8px 8px 8px;
    color: #440000;
    display: inline-block;
    font-size: 10px;
    height: 15px;
    margin: 0 0 0 12px;
    text-align: center;
    text-transform: uppercase;
    width: 15px;
}

.deleteImage a {
    color: #a20000;
    font-weight: bold;
    text-decoration: none;
}

.deleteImage a:hover {
    color: #FBDAC9;
    background-color: #7A0000;
    font-weight: bold;
    text-decoration: none;
}


</style>