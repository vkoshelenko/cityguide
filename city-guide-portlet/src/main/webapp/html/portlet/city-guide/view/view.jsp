<%@include file="init.jsp" %>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet</h2>
    </div>
    <div id="portletContent">
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <p>Some text here...</p>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>