<%@ taglib prefix="theme" uri="http://liferay.com/tld/theme" %>
<%@ include file="../init-common.jsp" %>

<theme:defineObjects/>

<portlet:renderURL var="defaultURL" portletMode="view" windowState="normal"/>

<portlet:renderURL var="defaultAjaxURL" portletMode="view" windowState="${exclusive}"/>

<portlet:renderURL var="deleteCategoryAjaxUrl" portletMode="view" windowState="${exclusive}">
    <portlet:param name="action" value="deleteCategory" />
</portlet:renderURL>

<%-- Home --%>
<c:set var="homeURL" value="${themeDisplay.portalURL}"/>

<%---- Categories ----%>
<portlet:renderURL var="addCategoryView">
    <portlet:param name="action" value="addCategoryView" />
</portlet:renderURL>

<portlet:renderURL var="addCategory">
    <portlet:param name="action" value="addCategory" />
</portlet:renderURL>

<portlet:renderURL var="editCategoryView">
    <portlet:param name="action" value="editCategoryView" />
</portlet:renderURL>

<portlet:renderURL var="editCategory">
    <portlet:param name="action" value="editCategory" />
</portlet:renderURL>

<portlet:renderURL var="deleteCategory">
    <portlet:param name="action" value="deleteCategory" />
</portlet:renderURL>

<portlet:renderURL var="categoriesListView">
    <portlet:param name="action" value="categoriesListView" />
</portlet:renderURL>


<%---- Ads ----%>
<portlet:renderURL var="addAdView">
    <portlet:param name="action" value="addAdView" />
</portlet:renderURL>

<portlet:actionURL var="addAd">
    <portlet:param name="action" value="addAd" />
</portlet:actionURL>

<portlet:renderURL var="editAdView">
    <portlet:param name="action" value="editAdView" />
</portlet:renderURL>

<portlet:actionURL var="editAd">
    <portlet:param name="action" value="editAd" />
</portlet:actionURL>

<portlet:renderURL var="deleteAd">
    <portlet:param name="action" value="deleteAd" />
</portlet:renderURL>

<portlet:renderURL var="searchAds">
    <portlet:param name="action" value="searchAds" />
</portlet:renderURL>

<portlet:renderURL var="filterAds">
    <portlet:param name="action" value="filterAds" />
</portlet:renderURL>

<portlet:renderURL var="deletePhoto" windowState="${exclusive}">
    <portlet:param name="action" value="deletePhoto" />
</portlet:renderURL>

<portlet:renderURL var="adsListView">
    <portlet:param name="action" value="adsListView" />
</portlet:renderURL>

<portlet:renderURL var="viewAdInfo">
    <portlet:param name="action" value="viewAdInfo" />
</portlet:renderURL>

<portlet:resourceURL var="viewImage">
    <portlet:param name="action" value="viewImage" />
</portlet:resourceURL>

