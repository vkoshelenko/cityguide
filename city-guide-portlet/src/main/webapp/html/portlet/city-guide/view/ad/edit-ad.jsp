<%@include file="../init.jsp" %>
<script type="text/javascript">
    jQuery(document).ready(function(){

        jQuery("#addPhoto").on("click", function() {
            var element = $(this);
            $('<div class="formRow"><div class="left">Photo:</div><div class="right"><input type="file" name="photo"  /><div class="deleteImage">x</div></div></div>')
                    .insertBefore(element);
        });

        jQuery(".photoBox").on("click", ".deleteImage", function() {
            var element = jQuery(this);
            var container = element.parent().parent();
            container.remove();
        });

        jQuery(".deleteImage a").on("click", function(event) {
            event.preventDefault();
            var isDelete = confirm("Do you want to delete this image?");
            if (isDelete) {
                var element = $(this);
                var url = element.attr("href");
                jQuery.ajax({
                    url: url,
                    method: 'POST',
                    data: {}
                }).done(function(response){
                    if (response.success === "true") {
                        var photoContainer = element.parent().parent();
                        photoContainer.remove();
                    }
                }).fail(function(response){
                    console.log("fail: " + JSON.stringify(response));
                });
            }
        });
    });
</script>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Edit Ad</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <h3 class="innerHeader">Edit Ad</h3>
            <form action="${editAd}" method="POST" enctype="multipart/form-data" class="addForm">
                <input type="hidden" name="id" value="${ad.id}" />
                <div class="formRow">
                    <div class="left">
                        Category:
                    </div>
                    <div class="right">
                        <select name="category.id">
                            <option value="-1">Select Category</option>
                            <c:forEach var="category" items="${categories}">
                                <option value="${category.id}" <c:if test="${category.id eq ad.category.id}"><c:out value="selected"/></c:if>>
                                    ${category.name}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Name:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['name'] ne null}">
                            <label class="error">${errorMap['name']}</label>
                        </c:if>
                        <input type="text" name="name" id="name" value="${ad.name}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        URL:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['url'] ne null}">
                            <label class="error">${errorMap['url']}</label>
                        </c:if>
                        <input type="text" name="url" value="${ad.url}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Address:
                    </div>
                    <div class="right">
                        <input type="text" name="address" value="${ad.address}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        City:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['city'] ne null}">
                            <label class="error">${errorMap['city']}</label>
                        </c:if>
                        <input type="text" name="city" value="${ad.city}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        State:
                    </div>
                    <div class="right">
                        <input type="text" name="state" value="${ad.state}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Country:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap ne null}">
                            <label class="error">${errorMap['country']}</label>
                        </c:if>
                        <input type="text" name="country" value="${ad.country}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        CEP:
                    </div>
                    <div class="right">
                        <input type="text" name="cep" value="${ad.cep}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Phone:
                    </div>
                    <div class="right">
                        <input type="text" name="phone" value="${ad.phone}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Phone2:
                    </div>
                    <div class="right">
                        <input type="text" name="phone2" value="${ad.phone2}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Description:
                    </div>
                    <div class="right">
                        <textarea rows="5" cols="10" name="description">${ad.description}</textarea>
                    </div>
                </div>
                <div class="photoBox">
                    <div class="formRow">
                        <div class="left">
                            Photo:
                        </div>
                        <div class="right">
                            <input type="file" name="photo"  />
                        </div>
                    </div>
                    <div id="addPhoto">Add more photo</div>
                </div>
                <div id="photoSection">
                    <div id="photoSectionTitle">Images of announcement:</div>
                    <c:forEach var="photo" items="${photos}">
                        <div class="photoWrapper">
                            <img src="${viewImage}&photoId=${photo.id}" width="200px" height="200px" />
                            <div class="deleteImage"><a href="${deletePhoto}&photoId=${photo.id}">x</a></div>
                        </div>
                    </c:forEach>
                </div>
                <div class="navigation">
                    <input type="button" value="Back" onclick="history.back();" />
                    <c:if test="${hasPermissions}">
                        <input type="submit" value="Edit Ad" id="editAdButton" />
                    </c:if>
                </div>
            </form>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>