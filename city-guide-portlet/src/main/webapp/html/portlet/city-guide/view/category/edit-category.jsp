<%@include file="../init.jsp" %>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Edit Category</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <h3 class="innerHeader">Edit Category</h3>
            <form action="${editCategory}" method="POST" class="addForm">
                <input type="hidden" name="id" value="${category.id}" />
                <div class="formRow">
                    <div class="left">
                        <label for="parentId">Parent category:</label>
                    </div>
                    <div class="right">
                        <select id="parentId" name="parentId">
                            <option value="-1">=== Select parent category ===</option>
                            <c:forEach var="parentCategory" items="${categories}">
                                <option value="${parentCategory.id}" <c:if test="${category.parentId eq parentCategory.id}"><c:out value="selected"/></c:if>>
                                ${parentCategory.name}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        <label for="name">Category name:</label>
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['name'] ne null}">
                            <label class="error">${errorMap['name']}</label>
                        </c:if>
                        <input type="text" id="name" name="name" value="${category.name}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Category description:
                    </div>
                    <div class="right">
                        <textarea rows="5" cols="10" name="description">${category.description}</textarea>
                    </div>
                </div>
                <div class="navigation">
                    <input type="button" value="Back" onclick="history.back();" />
                    <c:if test="${hasPermissions}">
                        <input type="submit" value="Edit Category" id="editCategoryButton" />
                    </c:if>
                </div>
            </form>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>