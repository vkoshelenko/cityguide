<%@include file="../init.jsp" %>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#deleteCategoryLink").on("click", function(event){
            var isDelete = confirm("Do you want to delete category?");
            if (!isDelete) {
                event.preventDefault();
            }
        });
    });
</script>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Category Info</h2>
    </div>
    <div id="portletContent">
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div class="innerContent">
            <h3 class="innerHeader">Category Information</h3>
            <div class="formRow">
                <div class="left">
                    Name:
                </div>
                <div class="right">
                    ${category.name}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Description:
                </div>
                <div class="right">
                    ${category.description}
                </div>
            </div>

            <c:if test="${not empty categoryAds}">
                <div class="items">
                    <table>
                        <tr>
                            <th colspan="4">
                                <h3 class="innerHeader">Category Ads</h3>
                            </th>
                        </tr>
                        <tr>
                            <th>N</th>
                            <th>Ad name</th>
                            <th>Ad description</th>
                            <th>Ad category</th>
                        </tr>
                            <c:set var="baseFriendlyUrl" value="<%= FriendlyURLUtils.buildBaseAdFriendlyUrl(renderRequest) %>" />
                            <c:forEach var="ad" items="${categoryAds}">
                                <tr>
                                    <td>${counter}.</td>
                                    <td><a href="${baseFriendlyUrl}/${ad.id}/${ad.name}">${ad.name}</a></td>
                                    <td>${ad.description}</td>
                                    <td>${ad.category.name}</td>
                                </tr>
                                <c:set var="counter" value="${counter + 1}" />
                            </c:forEach>
                    </table>
                </div>
            </c:if>

            <div class="navigation">
                <a href="#" onclick="history.back();">Back</a>
                <c:if test="${hasPermissions}">
                    <a href="${addCategoryView}">Add Category</a>
                    <a href="${editCategoryView}&categoryId=${category.id}">Edit</a>
                    <a href="${deleteCategory}&categoryId=${category.id}" id="deleteCategoryLink">Delete</a>
                </c:if>
            </div>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>