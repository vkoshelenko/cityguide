<%@include file="../init.jsp" %>
<script type="text/javascript">
    /*jQuery(document).ready(function(){
        jQuery("#categorySelect").on("change", function(event){
            var form = jQuery(".filterForm");
            form.submit();
        });
    });*/
</script>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Ads List</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                    ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                        ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                        ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <c:set var="baseFriendlyUrl" value="<%= FriendlyURLUtils.buildBaseAdFriendlyUrl(renderRequest) %>" />
            <c:set var="counter" value="1" />
            <div class="items">
                <div class="searchContainer">
                    <form action="${searchAds}" class="filterForm" method="POST">
                        <div class="formRow">
                            Search: &nbsp;&nbsp;&nbsp;
                            <input type="text" name="name" value="${searchParams.name}" />
                            &nbsp;&nbsp;&nbsp; in &nbsp;&nbsp;&nbsp;
                            <select name="categoryId" class="categorySelect">
                                <option value="-1">All Categories</option>
                                <c:forEach var="category" items="${categories}">
                                    <option value="${category.id}" <c:if test="${category.id eq searchParams.categoryId}"><c:out value="selected"/></c:if>>
                                            ${category.name}
                                    </option>
                                </c:forEach>
                            </select>
                            &nbsp;&nbsp;&nbsp;
                            <input type="submit" value="Search" />
                        </div>
                    </form>
                </div>
                <c:choose>
                    <c:when test="${not empty ads}">
                        <table>
                            <tr>
                                <th colspan="4">
                                    <h3 class="innerHeader">List of Ads</h3>
                                </th>
                            </tr>
                            <tr>
                                <th>N</th>
                                <th>Ad name</th>
                                <th>Ad description</th>
                                <th>Ad category</th>
                            </tr>
                            <c:forEach var="ad" items="${ads}">
                                <tr>
                                    <td>${counter}.</td>
                                    <td><a href="${baseFriendlyUrl}/${ad.id}/${ad.name}">${ad.name}</a></td>
                                    <td>${ad.description}</td>
                                    <td>${ad.category.name}</td>
                                </tr>
                                <c:set var="counter" value="${counter + 1}" />
                            </c:forEach>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <div class="emptyResults">
                            No records found.
                        </div>
                    </c:otherwise>
                </c:choose>
                <div class="searchContainer">
                    <form action="${searchAds}" class="filterForm" method="POST">
                        <div class="formRow">
                            Search: &nbsp;&nbsp;&nbsp;
                            <input type="text" name="name" value="${searchParams.name}" />
                            &nbsp;&nbsp;&nbsp; in &nbsp;&nbsp;&nbsp;
                            <select name="categoryId" class="categorySelect">
                                <option value="-1">All Categories</option>
                                <c:forEach var="category" items="${categories}">
                                    <option value="${category.id}" <c:if test="${category.id eq searchParams.categoryId}"><c:out value="selected"/></c:if>>
                                            ${category.name}
                                    </option>
                                </c:forEach>
                            </select>
                            &nbsp;&nbsp;&nbsp;
                            <input type="submit" value="Search" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="navigation">
                <a href="#" onclick="history.back();">Back</a>
                <c:if test="${hasPermissions}">
                    <a href="${addAdView}">Add an Ad</a>
                </c:if>
            </div>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>