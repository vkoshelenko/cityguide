<%@include file="../init.jsp" %>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var categoriesManager = new CategoriesManager({
            buttonClass: "itemButton"
        });
    });
</script>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Categories List</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <h3 class="innerHeader">List of Categories: </h3>
            <c:set var="baseFriendlyUrl" value="<%= FriendlyURLUtils.buildBaseCategoryFriendlyUrl(renderRequest) %>" />
            <c:set var="counter" value="1" />
            <div class="items">
                <c:forEach var="category" items="${categories}">
                <div class="item">
                    <c:choose>
                        <c:when test="${category.children ne null or category.children.size gt 0}">
                            <div class="itemButton">&nbsp;-&nbsp;</div>
                        </c:when>
                        <c:otherwise>
                            <div class="itemButtonInActive">&nbsp;&nbsp;&nbsp;</div>
                        </c:otherwise>
                    </c:choose>
                    <a href="${baseFriendlyUrl}/${category.id}/${category.name}">${category.name}"</a>
                    <c:set var="subCounter" value="1" />
                    <div class="subItems">
                        <c:forEach var="subCategory" items="${category.children}">
                        <div class="subItem">
                            <c:choose>
                                <c:when test="${subCategory.children ne null}">
                                    <div class="itemButton">&nbsp;-&nbsp;</div>
                                </c:when>
                                <c:otherwise>
                                    <div class="itemButtonInActive">&nbsp;&nbsp;&nbsp;</div>
                                </c:otherwise>
                            </c:choose>
                            <a href="${baseFriendlyUrl}/${subCategory.id}/${subCategory.name}">${subCategory.name}"</a>
                            <c:set var="subSubCounter" value="1" />
                            <div class="subSubItems">
                                <c:forEach var="subSubCategory" items="${subCategory.children}">
                                    <div class="subSubItem">
                                        <div class="itemButtonInActive">&nbsp;&nbsp;&nbsp;</div>
                                        <a href="${baseFriendlyUrl}/${subSubCategory.id}/${subSubCategory.name}">${subSubCategory.name}"</a>
                                        <c:set var="subSubCounter" value="${subSubCounter + 1}" />
                                    </div>
                                </c:forEach>
                            </div>
                            <c:set var="subCounter" value="${subCounter + 1}" />
                        </div>
                        </c:forEach>
                    </div>
                    <c:set var="counter" value="${counter + 1}" />
                </div>
                </c:forEach>
            </div>
            <div class="navigation">
                <a href="#" onclick="history.back();">Back</a>
                <c:if test="${hasPermissions}">
                    <a href="${addCategoryView}">Add Category</a>
                </c:if>
            </div>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>