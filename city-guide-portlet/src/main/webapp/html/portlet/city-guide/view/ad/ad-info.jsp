<%@include file="../init.jsp" %>
<script type="text/javascript">
    jQuery(document).ready(function() {
        $("#deleteAdLink").on("click", function(event){
            var isDelete = confirm("Do you want to delete ad?");
            if (!isDelete) {
                event.preventDefault();
            }
        });
    });
</script>
<div id="portletWrapper">
    <div id="portletHeader">
        <h2>City Guide Portlet - Ad Info</h2>
    </div>
    <div id="portletContent">
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div class="innerContent">
            <h3 class="innerHeader">Ads Information</h3>
            <div class="formRow">
                <div class="left">
                    Category:
                </div>
                <div class="right">
                    ${ad.category.name}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Name:
                </div>
                <div class="right">
                    ${ad.name}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Url:
                </div>
                <div class="right">
                    <a href="${ad.url}">${ad.url}</a>
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Address:
                </div>
                <div class="right">
                    ${ad.address}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    City:
                </div>
                <div class="right">
                    ${ad.city}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    State:
                </div>
                <div class="right">
                    ${ad.state}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Country:
                </div>
                <div class="right">
                    ${ad.country}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    CEP:
                </div>
                <div class="right">
                    ${ad.cep}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Phone:
                </div>
                <div class="right">
                    ${ad.phone}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Phone2:
                </div>
                <div class="right">
                    ${ad.phone2}
                </div>
            </div>
            <div class="formRow">
                <div class="left">
                    Description:
                </div>
                <div class="right">
                    ${ad.description}
                </div>
            </div>
            <div id="photoSection">
                <div id="photoSectionTitle">Images of announcement:</div>
                <c:forEach var="photo" items="${photos}">
                    <div class="photoWrapper">
                        <img src="${viewImage}&photoId=${photo.id}" width="200px" height="200px" />
                    </div>
                </c:forEach>
            </div>
            <div class="navigation">
                <a href="#" onclick="history.back();">Back</a>
                <c:if test="${hasPermissions}">
                    <a href="${addAdView}">Add Ad</a>
                    <a href="${editAdView}&adId=${ad.id}">Edit</a>
                    <a href="${deleteAd}&adId=${ad.id}" id="deleteAdLink">Delete</a>
                </c:if>
            </div>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>