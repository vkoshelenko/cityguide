<%@include file="../init.jsp" %>
<div id="portletWrapper">
    <script type="text/javascript">
        jQuery(document).ready(function(){

            jQuery("#addPhoto").on("click", function() {
                var element = jQuery(this);
                jQuery('<div class="formRow"><div class="left">Photo:</div><div class="right"><input type="file" name="photo"  /><div class="deleteImage">x</div></div></div>')
                .insertBefore(element);
            });

            jQuery(".photoBox").on("click", ".deleteImage", function() {
                var element = jQuery(this);
                var container = element.parent().parent();
                container.remove();
            });
        });
    </script>
    <div id="portletHeader">
        <h2>City Guide Portlet - Add an Ad</h2>
    </div>
    <div id="portletContent">
        <c:choose>
            <c:when test="${error ne null}">
                <div class="portlet-msg-error">
                        ${error}
                </div>
            </c:when>
            <c:otherwise>
                <c:if test="${info ne null}">
                    <div class="portlet-msg-info">
                            ${info}
                    </div>
                </c:if>
                <c:if test="${success ne null}">
                    <div class="portlet-msg-success">
                            ${success}
                    </div>
                </c:if>
            </c:otherwise>
        </c:choose>
        <div id="menuSection">
            <ul>
                |<li><a href="${categoriesListView}" id="categoriesLink">Categories</a></li> |
                <li><a href="${adsListView}" id="adsLink">Ads</a></li>|
            </ul>
        </div>
        <div class="innerContent">
            <h3 class="innerHeader">Add an ad:</h3>
            <form action="${addAd}" method="POST" enctype="multipart/form-data" class="addForm">
                <div class="formRow">
                    <div class="left">
                        Category:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['category'] ne null}">
                            <label class="error">${errorMap['category']}</label>
                        </c:if>
                        <select name="category.id">
                            <option value="-1">Select Category</option>
                            <c:forEach var="category" items="${categories}">
                                <option value="${category.id}" <c:if test="${category.id eq ad.category.id}"><c:out value="selected"/></c:if>>
                                    ${category.name}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Name:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['name'] ne null}">
                            <label class="error">${errorMap['name']}</label>
                        </c:if>
                        <input type="text" name="name" id="name" value="${ad.name}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        URL:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['url'] ne null}">
                            <label class="error">${errorMap['url']}</label>
                        </c:if>
                        <input type="text" name="url" value="${ad.url}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Address:
                    </div>
                    <div class="right">
                        <input type="text" name="address" value="${ad.address}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        City:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap['city'] ne null}">
                            <label class="error">${errorMap['city']}</label>
                        </c:if>
                        <input type="text" name="city" value="${ad ne null ? ad.city : preferences.defaultCity}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        State:
                    </div>
                    <div class="right">
                        <input type="text" name="state" value="${ad ne null ? ad.state : preferences.defaultState}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Country:
                    </div>
                    <div class="right">
                        <c:if test="${errorMap ne null}">
                            <label class="error">${errorMap['country']}</label>
                        </c:if>
                        <input type="text" name="country" value="${ad ne null ? ad.country : preferences.defaultCountry}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        CEP:
                    </div>
                    <div class="right">
                        <input type="text" name="cep" value="${ad.cep}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Phone:
                    </div>
                    <div class="right">
                        <input type="text" name="phone" value="${ad.phone}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Phone2:
                    </div>
                    <div class="right">
                        <input type="text" name="phone2" value="${ad.phone2}" />
                    </div>
                </div>
                <div class="formRow">
                    <div class="left">
                        Description:
                    </div>
                    <div class="right">
                        <textarea rows="5" cols="10" name="description">${ad.description}</textarea>
                    </div>
                </div>
                <div class="photoBox">
                    <div class="formRow">
                        <div class="left">
                            Photo:
                        </div>
                        <div class="right">
                            <input type="file" name="photo"  />
                        </div>
                    </div>
                    <div id="addPhoto">Add more photo</div>
                </div>
                <div class="navigation">
                    <input type="button" value="Back" onclick="history.back();" />
                    <c:if test="${hasPermissions}">
                        <input type="submit" value="Add Ad" />
                    </c:if>
                </div>
            </form>
        </div>
    </div>
    <div id="portletFooter">
        &copy; Company name, 2012
    </div>
</div>