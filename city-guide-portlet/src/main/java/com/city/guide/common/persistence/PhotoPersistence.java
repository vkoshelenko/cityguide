package com.city.guide.common.persistence;

import com.city.guide.common.model.impl.Photo;

import java.util.List;

public interface PhotoPersistence extends Persistence<Photo> {

    List<Photo> getAdPhotos(Long adId);

    Long getAdPhotosCount(Long adId);

}
