package com.city.guide.common.model.impl;

public class SearchParams {

    private Long categoryId;

    private String name;

    public SearchParams() {
    }

    public SearchParams(Long categoryId, String name) {
        this.categoryId = categoryId;
        this.name = name;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
