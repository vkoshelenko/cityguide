package com.city.guide.common.security;

public interface CityGuideRoles {

    String CITY_GUIDE_ADMIN = "CityGuideAdministrator";

    String CITY_GUIDE_EDITOR = "CityGuideEditor";


    String CITY_GUIDE_ADMIN_GROUP = "CityGuideAdministrators";

    String CITY_GUIDE_EDITOR_GROUP = "CityGuideEditors";
}
