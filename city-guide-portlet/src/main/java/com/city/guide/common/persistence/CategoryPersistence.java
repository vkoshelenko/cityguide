package com.city.guide.common.persistence;

import com.city.guide.common.model.impl.Category;

import java.util.List;

public interface CategoryPersistence extends Persistence<Category> {

    List<Category> getRootCategories();

    Category getParentCategory(Long categoryId);

}
