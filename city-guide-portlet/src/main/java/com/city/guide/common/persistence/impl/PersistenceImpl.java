package com.city.guide.common.persistence.impl;

import com.city.guide.common.model.HibernateModel;
import com.city.guide.common.persistence.Persistence;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public class PersistenceImpl<E> extends HibernateDaoSupport implements Persistence<E> {

    @Override
    @SuppressWarnings("unchecked")
    public <E extends HibernateModel> E get(Class<E> clazz, Serializable id) throws DataAccessException {
        return (E)getSessionFactory().getCurrentSession().get(clazz,id);
    }

    @Override
    public <E extends HibernateModel> List getAll(Class<E> clazz) {
        return getSessionFactory().getCurrentSession().createQuery("select c from " + clazz.getSimpleName() + " as c").list();
    }

    @Override
    public void insert(HibernateModel object) throws DataAccessException {
        getSessionFactory().getCurrentSession().save(object);
        getSessionFactory().getCurrentSession().flush();
    }

    @Override
    public void update(HibernateModel object) throws DataAccessException {
        getSessionFactory().getCurrentSession().update(object);
        getSessionFactory().getCurrentSession().flush();
        getSessionFactory().getCurrentSession().clear();
    }

    @Override
    public void merge(HibernateModel object) throws DataAccessException {
        getSessionFactory().getCurrentSession().merge(object);
        getSessionFactory().getCurrentSession().flush();
        getSessionFactory().getCurrentSession().clear();
    }

    @Override
    public void delete(HibernateModel object) throws DataAccessException {
        getSessionFactory().getCurrentSession().delete(object);
        getSessionFactory().getCurrentSession().flush();
    }

    @Override
    public <E extends HibernateModel> void deleteById(Class<E> clazz, Serializable id) throws DataAccessException {
        HibernateModel hibernateModel = get(clazz,id);
        delete(hibernateModel);
    }

    @Override
    public <E extends HibernateModel> void deleteAll(List<E> objects) throws DataAccessException {
        for(HibernateModel hibernateModel : objects){
            delete(hibernateModel);
        }
    }
}
