package com.city.guide.common.persistence.impl;

import com.city.guide.common.model.impl.Photo;
import com.city.guide.common.persistence.PhotoPersistence;
import org.hibernate.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoPersistenceImpl extends PersistenceImpl<Photo> implements PhotoPersistence {

    @Override
    public List<Photo> getAdPhotos(Long adId) {
        Query query = getSessionFactory().getCurrentSession().createQuery("select p from Photo p where p.ad.id = :adId");
        query.setParameter("adId", adId);
        return query.list();
    }

    @Override
    public Long getAdPhotosCount(Long adId) {
        Query query = getSessionFactory().getCurrentSession().createQuery("select count(*) from Photo p where p.ad.id = :adId");
        query.setParameter("adId", adId);
        return (Long)query.uniqueResult();
    }
}
