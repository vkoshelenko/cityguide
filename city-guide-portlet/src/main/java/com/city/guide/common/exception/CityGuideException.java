package com.city.guide.common.exception;

public class CityGuideException extends Exception {

    public CityGuideException() {
    }

    public CityGuideException(String message) {
        super(message);
    }

    public CityGuideException(String message, Throwable cause) {
        super(message, cause);
    }

    public CityGuideException(Throwable cause) {
        super(cause);
    }
}
