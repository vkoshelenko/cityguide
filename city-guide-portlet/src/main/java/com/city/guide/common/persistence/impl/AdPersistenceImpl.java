package com.city.guide.common.persistence.impl;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.SearchParams;
import com.city.guide.common.persistence.AdPersistence;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdPersistenceImpl extends PersistenceImpl<Ad> implements AdPersistence {

    @Override
    public List<Ad> getCategoryAds(Long categoryId) {
        Query query = getSessionFactory().getCurrentSession().createQuery("select ad from Ad ad where ad.category.id = :categoryId");
        query.setParameter("categoryId", categoryId);
        return query.list();
    }

    @Override
    public List<Ad> searchAds(SearchParams searchParams) {

        Map<String, Object> paramMap = new HashMap<String, Object>();

        StringBuilder sql = new StringBuilder("select ad from Ad ad ");

        boolean isFirstParam = true;

        Long categoryId = searchParams.getCategoryId();
        if (categoryId > 0) {
            sql.append(" where ad.category.id = :categoryId");
            paramMap.put("categoryId", categoryId);
            isFirstParam = false;
        }

        String name = searchParams.getName();
        if (StringUtils.isNotBlank(name)) {
            sql.append(isFirstParam ? " where " : " and ");
            sql.append("(ad.name like :name or ad.url like :name or ad.description like :name)");
            paramMap.put("name", wrapSearchParam(name));
        }

        Query query = getSessionFactory().getCurrentSession().createQuery(sql.toString());
        for (String key : paramMap.keySet()) {
            Object value = paramMap.get(key);
            query.setParameter(key, value);
        }

        return query.list();
    }

    private String wrapSearchParam(String name) {
        return "%" + name + "%";
    }
}
