package com.city.guide.common.persistence;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.SearchParams;

import java.util.List;

public interface AdPersistence extends Persistence<Ad> {

    List<Ad> getCategoryAds(Long categoryId);

    List<Ad> searchAds(SearchParams searchParams);
}
