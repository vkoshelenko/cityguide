package com.city.guide.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service("props")
public class ApplicationPropsBean {

    @Value("${default.country}")
    private String defaultCountry;

    @Value("${default.state}")
    private String defaultState;

    @Value("${default.city}")
    private String defaultCity;

    @Value("${max.images.count}")
    private Integer maxImagesCount;

    @Value("${default.company.id}")
    private Long defaultCompanyId;

    @Value("${default.admin.user.id}")
    private Long defaultAdminUserId;

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public String getDefaultState() {
        return defaultState;
    }

    public String getDefaultCity() {
        return defaultCity;
    }

    public Integer getMaxImagesCount() {
        return maxImagesCount;
    }

    public void setMaxImagesCount(Integer maxImagesCount) {
        this.maxImagesCount = maxImagesCount;
    }

    public Long getDefaultCompanyId() {
        return defaultCompanyId;
    }

    public void setDefaultCompanyId(Long defaultCompanyId) {
        this.defaultCompanyId = defaultCompanyId;
    }

    public Long getDefaultAdminUserId() {
        return defaultAdminUserId;
    }

    public void setDefaultAdminUserId(Long defaultAdminUserId) {
        this.defaultAdminUserId = defaultAdminUserId;
    }
}
