package com.city.guide.common.service.impl;

import com.city.guide.common.model.impl.Photo;
import com.city.guide.common.persistence.PhotoPersistence;
import com.city.guide.common.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoServiceImpl implements PhotoService {

    @Autowired
    private PhotoPersistence photoPersistence;

    @Override
    public Photo getPhoto(Long photoId) {
        return photoPersistence.get(Photo.class, photoId);
    }

    @Override
    public List<Photo> getAllPhotos() {
        return photoPersistence.getAll(Photo.class);
    }

    @Override
    public List<Photo> getAdPhotos(Long adId) {
        return photoPersistence.getAdPhotos(adId);
    }

    @Override
    public Long getAdPhotosCount(Long adId) {
        return photoPersistence.getAdPhotosCount(adId);
    }

    @Override
    public void savePhoto(Photo photo) {
        photoPersistence.insert(photo);
    }

    @Override
    public void updatePhoto(Photo photo) {
        photoPersistence.update(photo);
    }

    @Override
    public void deletePhoto(Photo photo) {
        photoPersistence.delete(photo);
    }

    @Override
    public void deletePhoto(Long photoId) {
        photoPersistence.deleteById(Photo.class, photoId);
    }
}
