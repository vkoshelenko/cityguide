package com.city.guide.common.service.impl;

import com.city.guide.common.model.impl.Category;
import com.city.guide.common.persistence.CategoryPersistence;
import com.city.guide.common.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryPersistence categoryPersistence;

    @Override
    public Category getCategory(Long id) {
        return categoryPersistence.get(Category.class, id);
    }

    @Override
    public Category getParentCategory(Long categoryId) {
        return categoryPersistence.getParentCategory(categoryId);
    }

    @Override
    public List<Category> getCategories() {
        return categoryPersistence.getAll(Category.class);
    }

    @Override
    public void saveCategory(Category category) {
        categoryPersistence.insert(category);
    }

    @Override
    public void updateCategory(Category category) {
        categoryPersistence.update(category);
    }

    @Override
    public void deleteCategory(Category category) {
        categoryPersistence.delete(category);
    }

    @Override
    public void deleteCategory(Long categoryId) {
        categoryPersistence.deleteById(Category.class, categoryId);
    }

    @Override
    public List<Category> getRootCategories() {
        return categoryPersistence.getRootCategories();
    }
}
