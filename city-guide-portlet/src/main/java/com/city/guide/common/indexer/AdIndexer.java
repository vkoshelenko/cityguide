package com.city.guide.common.indexer;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.service.AdService;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;

import javax.portlet.PortletURL;
import java.util.Locale;

public class AdIndexer extends BaseIndexer {

    public static final String[] CLASS_NAMES = {Ad.class.getName()};

    public static final String PORTLET_ID = "cityguide_WAR_cityguide";

    public static final long COMPANY_ID = 10154;

    public static final long GROUP_ID = 10180;

    public static final long USER_ID = 10158;

    @Autowired
    private AdService adService;

    @Override
    protected void doDelete(Object obj) throws Exception {

        Ad ad = (Ad)obj;
        Document document = new DocumentImpl();

        document.addUID(PORTLET_ID, ad.getId());
        SearchEngineUtil.deleteDocument(COMPANY_ID, document.get(Field.UID));
    }

    @Override
    protected Document doGetDocument(Object obj) throws Exception {

        Ad ad = (Ad)obj;

        String title = ad.getName();
        String description = ad.getDescription();
        long resourcePrimKey = ad.getId();

        Document document = new DocumentImpl();
        document.addUID(PORTLET_ID, resourcePrimKey);
        document.addKeyword(Field.PORTLET_ID, PORTLET_ID);
        document.addText(Field.TITLE, title);
        document.addText(Field.DESCRIPTION, description);

        return document;
    }

    @Override
    protected Summary doGetSummary(Document document, Locale locale, String snippet, PortletURL portletURL) throws Exception {

        String title = document.get(Field.TITLE);

        String content = snippet;

        if (Validator.isNull(snippet)) {
            content = document.get(Field.DESCRIPTION);

            if (Validator.isNull(content)) {
                content = StringUtil.shorten(document.get(Field.CONTENT), 200);
            }
        }

        String resourcePrimKey = document.get(Field.ENTRY_CLASS_PK);

        portletURL.setParameter("jspPage", "/html/portlet/city-guide/view/ad/ads-list.jsp");
        portletURL.setParameter("resourcePrimKey", resourcePrimKey);

        return new Summary(title, content, portletURL);
    }

    @Override
    protected void doReindex(Object obj) throws Exception {
        Ad ad = (Ad)obj;
        SearchEngineUtil.updateDocument(COMPANY_ID, getDocument(ad));
    }

    @Override
    protected void doReindex(String className, long classPK) throws Exception {
        Ad ad = adService.getAd(classPK);
        doReindex(ad);
    }

    @Override
    protected void doReindex(String[] ids) throws Exception {
        long companyId = GetterUtil.getLong(ids[0]);
    }

    @Override
    protected String getPortletId(SearchContext searchContext) {
        return PORTLET_ID;
    }

    @Override
    public String[] getClassNames() {
        return CLASS_NAMES;
    }

    @Override
    public String getPortletId() {
        return PORTLET_ID;
    }
}
