package com.city.guide.common.service;

import com.city.guide.common.model.impl.Photo;

import java.util.List;

public interface PhotoService {

    Photo getPhoto(Long photoId);

    List<Photo> getAllPhotos();

    List<Photo> getAdPhotos(Long adId);

    Long getAdPhotosCount(Long adId);

    void savePhoto(Photo photo);

    void updatePhoto(Photo photo);

    void deletePhoto(Photo photo);

    void deletePhoto(Long photoId);
}
