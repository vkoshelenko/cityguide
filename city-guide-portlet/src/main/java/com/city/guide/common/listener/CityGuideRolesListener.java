package com.city.guide.common.listener;

import com.city.guide.common.security.CityGuideRoles;
import com.liferay.portal.kernel.deploy.hot.HotDeployEvent;
import com.liferay.portal.kernel.deploy.hot.HotDeployException;
import com.liferay.portal.kernel.deploy.hot.HotDeployListener;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class CityGuideRolesListener implements HotDeployListener {

    private static final Logger _logger = Logger.getLogger(CityGuideRolesListener.class);

    @Override
    public void invokeDeploy(HotDeployEvent event) throws HotDeployException {

        _logger.info("CityGuideRolesListener start");

        //Obtain company Id
        long companyId = 10154;
        long[] companyIds = PortalUtil.getCompanyIds();
        if (companyIds != null && companyIds.length > 0) {
            companyId = companyIds[0];
        }

        //Obtain userId
        long userId = 10196;
        try {
            Role adminRole = RoleLocalServiceUtil.getRole(companyId, RoleConstants.ADMINISTRATOR);
            List<User> roleUsers = UserLocalServiceUtil.getRoleUsers(adminRole.getRoleId());
            if (roleUsers != null && roleUsers.size() > 0) {
                userId = roleUsers.get(0).getUserId();
            }
        } catch (Exception e) {
            _logger.error("Can not get role: " + e.getMessage());
        }

        /*=== CityGuideRoles.CITY_GUIDE_ADMIN ===*/
        Role cityGuideAdminRole = null;
        try {
            cityGuideAdminRole = RoleLocalServiceUtil.getRole(companyId, CityGuideRoles.CITY_GUIDE_ADMIN);
        } catch (Exception e) {
            _logger.error("Error when obtaining role " + CityGuideRoles.CITY_GUIDE_ADMIN + ": " + e.getMessage());
        }
        if (cityGuideAdminRole == null) {
            try {
                RoleLocalServiceUtil.addRole(
                        userId,
                        companyId,
                        CityGuideRoles.CITY_GUIDE_ADMIN,
                        new HashMap<Locale, String>(),
                        new HashMap<Locale, String>(),
                        RoleConstants.TYPE_REGULAR
                );
            } catch (Exception e) {
                _logger.error("Can not add role  " + CityGuideRoles.CITY_GUIDE_ADMIN + ": " + e.getMessage());
            }
        }

        /*=== CityGuideRoles.CITY_GUIDE_EDITOR ===*/
        Role cityGuideEditorRole = null;
        try {
            cityGuideEditorRole = RoleLocalServiceUtil.getRole(companyId, CityGuideRoles.CITY_GUIDE_EDITOR);
        } catch (Exception e) {
            _logger.error("Error when obtaining role " + CityGuideRoles.CITY_GUIDE_EDITOR + ": " + e.getMessage());
        }
        if (cityGuideEditorRole == null) {
            try {
                RoleLocalServiceUtil.addRole(
                        userId,
                        companyId,
                        CityGuideRoles.CITY_GUIDE_EDITOR,
                        new HashMap<Locale, String>(),
                        new HashMap<Locale, String>(),
                        RoleConstants.TYPE_REGULAR
                );
            } catch (Exception e) {
                _logger.error("Can not add role  " + CityGuideRoles.CITY_GUIDE_EDITOR + ": " + e.getMessage());
            }
        }

        _logger.info("CityGuideRolesListener end");
    }

    @Override
    public void invokeUndeploy(HotDeployEvent event) throws HotDeployException {
    }
}
