package com.city.guide.common.model.impl;

import com.city.guide.common.model.BaseModel;
import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "city_guide_ad")
public class Ad extends BaseModel {

    @NotNull(errorCode = "name", message = "Ad name is required field.")
    @NotEmpty(errorCode = "name", message = "Ad name is required field.")
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull(errorCode = "url", message = "Ad url is required field.")
    @NotEmpty(errorCode = "url", message = "Ad url is required field.")
    @Column(name = "url")
    private String url;

    @Column(name = "address")
    private String address;

    @NotNull(errorCode = "city", message = "Ad city is required field.")
    @NotEmpty(errorCode = "city", message = "Ad city is required field.")
    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @NotNull(errorCode = "country", message = "Ad country is required field.")
    @NotEmpty(errorCode = "country", message = "Ad country is required field.")
    @Column(name = "country")
    private String country;

    @Column(name = "cep")
    private String cep;

    @Column(name = "phone")
    private String phone;

    @Column(name = "phone2")
    private String phone2;

    @Lob
    @Column(name = "description")
    private String description;

    @NotNull(errorCode = "category", message = "Category is required field.")
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "categoryId", nullable = false)
    private Category category;

    @OneToMany(mappedBy = "ad", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Photo> photos;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }
}
