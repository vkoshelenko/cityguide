package com.city.guide.common.service;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.SearchParams;

import java.util.List;

public interface AdService {

    Ad getAd(Long adId);

    List<Ad> getAds();

    List<Ad> getCategoryAds(Long categoryId);

    List<Ad> searchAds(SearchParams searchParams);

    void saveAd(Ad ad);

    void updateAd(Ad ad);

    void deleteAd(Ad ad);

    void deleteAd(Long adId);
}
