package com.city.guide.common.security;

import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.RoleConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserGroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import org.apache.log4j.Logger;

import javax.portlet.PortletRequest;

/**
 * Checks user's roles
 */
public class RoleChecker {

    private static Logger _logger = Logger.getLogger(RoleChecker.class);

    /**
     * Checks if user can manage categories
     *
     * @param portletRequest - PortletRequest
     * @return - if user can manage categories
     */
    public static boolean hasCategoryManagementPermissions(PortletRequest portletRequest) {

        ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

        long companyId = themeDisplay.getCompanyId();

        User user = themeDisplay.getUser();

        return  user != null &&
                (hasUserRole(companyId, user, RoleConstants.ADMINISTRATOR) ||
                 hasUserRole(companyId, user, CityGuideRoles.CITY_GUIDE_ADMIN) ||
                 hasUserGroup(companyId, user, CityGuideRoles.CITY_GUIDE_ADMIN_GROUP));
    }

    /**
     * Checks if user can manage ads
     *
     * @param portletRequest - PortletRequest
     * @return - if user can manage ads
     */
    public static boolean hasAdsManagementPermissions(PortletRequest portletRequest) {

        ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);

        long companyId = themeDisplay.getCompanyId();

        User user = themeDisplay.getUser();

        return  user != null &&
                (hasUserRole(companyId, user, RoleConstants.ADMINISTRATOR) ||
                 hasUserRole(companyId, user, CityGuideRoles.CITY_GUIDE_ADMIN) ||
                 hasUserRole(companyId, user, CityGuideRoles.CITY_GUIDE_EDITOR) ||
                 hasUserGroup(companyId, user, CityGuideRoles.CITY_GUIDE_ADMIN_GROUP) ||
                 hasUserGroup(companyId, user, CityGuideRoles.CITY_GUIDE_EDITOR_GROUP));
    }


    /**
     * Checks if user has the role
     *
     * @param companyId - current company Id
     * @param user - current user
     * @param roleName - role name to check
     * @return - if user has role
     */
    private static boolean hasUserRole(long companyId, User user, String roleName) {
        boolean hasRole = false;
        try {
            hasRole = RoleLocalServiceUtil.hasUserRole(user.getUserId(), companyId, roleName, false);
        } catch (Exception e) {
            _logger.error("Can not check user role: " + e.getMessage());
        }
        return hasRole;
    }


    /**
     * Checks if user is member of group
     *
     * @param companyId - current company Id
     * @param user - current user
     * @param groupName - group name to check
     * @return - if user is group's member
     */
    private static boolean hasUserGroup(long companyId, User user, String groupName)  {
        boolean hasGroup = false;
        try {
            UserGroup userGroup = UserGroupLocalServiceUtil.getUserGroup(companyId, groupName);
            hasGroup = UserLocalServiceUtil.hasUserGroupUser(userGroup.getUserGroupId(), user.getUserId());
        } catch (Exception e) {
            _logger.error("Can not check user group: " + e.getMessage());
        }
        return hasGroup;
    }
}
