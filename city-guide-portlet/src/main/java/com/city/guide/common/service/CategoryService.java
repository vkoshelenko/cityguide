package com.city.guide.common.service;

import com.city.guide.common.model.impl.Category;
import com.city.guide.web.model.ui.UICategory;

import java.util.List;

public interface CategoryService {

    Category getCategory(Long categoryId);

    Category getParentCategory(Long categoryId);

    List<Category> getCategories();

    void saveCategory(Category category);

    void updateCategory(Category category);

    void deleteCategory(Category category);

    void deleteCategory(Long categoryId);

    List<Category> getRootCategories();
}
