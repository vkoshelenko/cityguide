package com.city.guide.common.service.impl;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.SearchParams;
import com.city.guide.common.persistence.AdPersistence;
import com.city.guide.common.service.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdServiceImpl implements AdService {

    @Autowired
    private AdPersistence adPersistence;

    @Override
    public Ad getAd(Long adId) {
        return adPersistence.get(Ad.class, adId);
    }

    @Override
    public List<Ad> getAds() {
        return adPersistence.getAll(Ad.class);
    }

    @Override
    public List<Ad> getCategoryAds(Long categoryId) {
        return adPersistence.getCategoryAds(categoryId);
    }

    @Override
    public List<Ad> searchAds(SearchParams searchParams) {
        return adPersistence.searchAds(searchParams);
    }

    @Override
    public void saveAd(Ad ad) {
        adPersistence.insert(ad);
    }

    @Override
    public void updateAd(Ad ad) {
        adPersistence.update(ad);
    }

    @Override
    public void deleteAd(Ad ad) {
        adPersistence.delete(ad);
    }

    @Override
    public void deleteAd(Long adId) {
        adPersistence.deleteById(Ad.class, adId);
    }
}
