package com.city.guide.common.persistence.impl;

import com.city.guide.common.model.impl.Category;
import com.city.guide.common.persistence.CategoryPersistence;
import org.hibernate.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryPersistenceImpl extends PersistenceImpl<Category> implements CategoryPersistence {

    @Override
    public List<Category> getRootCategories() {
        Query query = getSessionFactory().getCurrentSession().createQuery("select c from Category c where c.parent <= 0 or c.parent = NULL order by c.name");
        return query.list();
    }

    @Override
    public Category getParentCategory(Long categoryId) {
        Query query = getSessionFactory().getCurrentSession()
                .createSQLQuery("select * from city_guide_category c where c.id = (select parentId from city_guide_category c where c.id = :categoryId)")
                .addEntity(Category.class);
        query.setParameter("categoryId", categoryId);
        List resultList = query.list();
        return (resultList != null && resultList.size() > 0) ?
                (Category)resultList.get(0) :
                null;
    }
}
