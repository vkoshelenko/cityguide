package com.city.guide.web.util;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.springframework.web.servlet.view.AbstractView;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import java.text.SimpleDateFormat;
import java.util.Map;

public class JacksonViewHelper {

    private final static Logger logger = Logger.getLogger(JacksonViewHelper.class);


    private JacksonViewHelper() {
    }

    public static AbstractView getJsonView(Object object) {

        logger.debug("Building json success result from object");

        if (object == null) {

            logger.debug("Result object is null, create dummy instance");

            object = new Object();

        }

        if (object instanceof Map) {

            return getJsonView((Map<String, Object>)object);

        }

        final Map<String,Object> map;

        try {

            map = BeanUtils.describe(object);

        } catch (Exception e) {

            logger.debug("Unable to get object properties", e);

            throw new RuntimeException(e);

        }

        return getJsonView(map);
    }

    public static MappingJacksonJsonView getJsonView(String key, Object value) {

        final MappingJacksonJsonView mappingJacksonJsonView = initJackson();

        mappingJacksonJsonView.addStaticAttribute(key, value);

        return mappingJacksonJsonView;
    }

    public static MappingJacksonJsonView getJsonView(Map<String, Object> objects) {

        final MappingJacksonJsonView mappingJacksonJsonView = initJackson();

        for (String key : objects.keySet()) {
            mappingJacksonJsonView.addStaticAttribute(key, objects.get(key));
        }
        return mappingJacksonJsonView;
    }

    private static MappingJacksonJsonView initJackson(){
        MappingJacksonJsonView mappingJacksonJsonView = new MappingJacksonJsonView();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.getSerializationConfig().withDateFormat( new SimpleDateFormat("MM/dd/yyyy"));
        mappingJacksonJsonView.setObjectMapper(objectMapper);
        return mappingJacksonJsonView;
    }
}
