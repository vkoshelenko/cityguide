package com.city.guide.web.util;

import com.city.guide.common.model.impl.Category;
import com.city.guide.web.model.ui.UICategory;
import net.sf.oval.ConstraintViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CityGuideUtils {

    /**
     * Transforms list of DAO models to list of UI models
     *
     * @param categories - list of root categories
     * @return uiCategories - list of UICategory objects
     */
    public static List<UICategory> getUICategories(List<Category> categories) {
        if (categories == null || categories.size() == 0) {
            return null;
        }

        List<UICategory> uiCategories = new ArrayList<>();

        for (Category category : categories) {

            UICategory uiCategory = new UICategory();
            uiCategory.setId(category.getId());
            uiCategory.setName(category.getName());
            uiCategory.setDescription(category.getDescription());

            List<Category> childrenCategories = category.getChildren();

            List<UICategory> childrenUICategories = getUICategories(childrenCategories);

            uiCategory.setChildren(childrenUICategories);
            uiCategories.add(uiCategory);
        }

        return uiCategories;
    }


    /**
     * @param violations - ConstraintViolation
     * @return - map of errors
     */
    public static Map<String, String> getErrorMap(List<ConstraintViolation> violations){

        Map<String, String> errorMap = new HashMap<String, String>();

        for (ConstraintViolation violation : violations) {
            String message = violation.getMessage();
            String code = violation.getErrorCode();
            errorMap.put(code, message);
        }

        return errorMap;
    }


}
