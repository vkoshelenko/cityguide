package com.city.guide.web.util;

public interface FriendlyURLConstants {

    String AD_PARAMETER = "cg_ad_id";
    String AD_NAME = "city-guide/ad";
    String AD_URL_START = "/city-guide/ad/";
    Integer AD_PARAMETER_INDEX = 3;


    String CATEGORY_PARAMETER = "cg_ct_id";
    String CATEGORY_NAME = "city-guide/category";
    String CATEGORY_URL_START = "/city-guide/category/";
    Integer CATEGORY_PARAMETER_INDEX = 3;
}
