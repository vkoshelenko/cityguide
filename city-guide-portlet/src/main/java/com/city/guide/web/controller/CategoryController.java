package com.city.guide.web.controller;

import com.city.guide.common.model.impl.Category;
import com.city.guide.common.security.RoleChecker;
import com.city.guide.web.model.rest.CategoryModel;
import com.city.guide.web.model.ui.UICategory;
import com.city.guide.web.util.CityGuideUtils;
import net.sf.oval.ConstraintViolation;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.RenderRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("VIEW")
public class CategoryController extends BaseController {

    protected Logger _logger = Logger.getLogger(getClass());

    /**
     * Obtain ModelAndView for categories list
     *
     * @return - modelAndView
     */
    @RenderMapping(params = "action=categoriesListView")
    public ModelAndView getCategoriesView(RenderRequest renderRequest) {
        ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
        List<Category> rootCategories = categoryService.getRootCategories();
        List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
        modelAndView.addObject("categories", categories);
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }


    /**
     * Obtain ModelAndView for adding category
     *
     * @return modelAndView
     */
    @RenderMapping(params = "action=addCategoryView")
    public ModelAndView addCategoryView(RenderRequest renderRequest){
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            modelAndView.addObject("info", "You have no  permissions for this action.");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView(CATEGORY_ADD_VIEW);
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Obtain ModelAndView for updating category
     *
     * @param categoryId - id of category
     * @return - modelAndView
     */
    @RenderMapping(params = "action=editCategoryView")
    public ModelAndView editCategoryView(RenderRequest renderRequest,
                                         @RequestParam("categoryId") Long categoryId){
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            modelAndView.addObject("info", "You have no  permissions for this action.");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView(CATEGORY_EDIT_VIEW);
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        Category category = categoryService.getCategory(categoryId);
        Category parent = categoryService.getParentCategory(category.getId());
        long parentId = parent != null ? parent.getId() : 0;
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setId(category.getId());
        categoryModel.setName(category.getName());
        categoryModel.setDescription(category.getDescription());
        categoryModel.setParentId(parentId);
        modelAndView.addObject("category", categoryModel);
        modelAndView.addObject("parentId", parentId);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Adds a category
     *
     * @param categoryModel - CategoryModel
     * @return modelAndView
     */
    @RenderMapping(params = "action=addCategory")
    public ModelAndView addCategory(RenderRequest renderRequest,
                                    @ModelAttribute(value = "category") CategoryModel categoryModel) {
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            modelAndView.addObject("info", "You have no  permissions for this action.");
            return modelAndView;
        }
        List<ConstraintViolation> violations = ovalValidator.validate(categoryModel);
        ModelAndView modelAndView = new ModelAndView(CATEGORY_ADD_VIEW);
        modelAndView.addObject("hasPermissions", hasPermissions);
        if (!violations.isEmpty()){
            Map<String,String> errorMap = CityGuideUtils.getErrorMap(violations);
            modelAndView.addObject("errorMap", errorMap);
            List<Category> categories = categoryService.getCategories();
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("category", categoryModel);
        } else {
            Category category = new Category();
            category.setName(categoryModel.getName());
            category.setDescription(categoryModel.getDescription());
            long parentId = categoryModel.getParentId();
            if (parentId > 0) {
                Category parentCategory = categoryService.getCategory(parentId);
                category.setParent(parentCategory);
            }
            categoryService.saveCategory(category);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.setViewName(CATEGORIES_LIST_VIEW);
            modelAndView.addObject("success", "Category has been saved successfully.");
        }
        return modelAndView;
    }

    /**
     * Updates the category
     *
     * @param categoryModel - CategoryModel
     * @return modelAndView
     */
    @RenderMapping(params = "action=editCategory")
    public ModelAndView editCategory(RenderRequest renderRequest,
                                     @ModelAttribute(value = "category") CategoryModel categoryModel){
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            modelAndView.addObject("info", "You have no  permissions for this action.");
            return modelAndView;
        }
        List<ConstraintViolation> violations = ovalValidator.validate(categoryModel);
        ModelAndView modelAndView = new ModelAndView(CATEGORY_EDIT_VIEW);
        modelAndView.addObject("hasPermissions", hasPermissions);
        if (!violations.isEmpty()){
            Map<String,String> errorMap = CityGuideUtils.getErrorMap(violations);
            modelAndView.addObject("errorMap", errorMap);
            List<Category> categories = categoryService.getCategories();
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("category", categoryModel);
        } else {
            long categoryId = categoryModel.getId();
            Category category = categoryService.getCategory(categoryId);
            if (category == null) {
                modelAndView.setViewName(CATEGORY_EDIT_VIEW);
                modelAndView.addObject("error", "Category was not found.");
                return modelAndView;
            }
            category.setName(categoryModel.getName());
            category.setDescription(categoryModel.getDescription());
            long parentId = categoryModel.getParentId();
            if (parentId > 0) {
                Category parentCategory = categoryService.getCategory(parentId);
                category.setParent(parentCategory);
            }
            categoryService.updateCategory(category);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.setViewName(CATEGORIES_LIST_VIEW);
            modelAndView.addObject("success", "Category has been saved successfully.");
        }
        return modelAndView;
    }


    /**
     * Deletes the category
     *
     * @param categoryId - id of category
     * @return modelAndView
     */
    @RenderMapping(params = "action=deleteCategory")
    public ModelAndView deleteCategory(RenderRequest renderRequest,
                                       @RequestParam("categoryId") Long categoryId) {
        //check permissions
        boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            modelAndView.addObject("info", "You have no  permissions for this action.");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView(CATEGORIES_LIST_VIEW);
        modelAndView.addObject("hasPermissions", hasPermissions);
        try {
            categoryService.deleteCategory(categoryId);
            modelAndView.addObject("success", "Category has been deleted successfully.");
            List<Category> rootCategories = categoryService.getRootCategories();
            List<UICategory> categories = CityGuideUtils.getUICategories(rootCategories);
            modelAndView.addObject("categories", categories);
        } catch (Exception e) {
            String errorMsg = "Can not delete category: " + e.getMessage();
            _logger.error(errorMsg);
            modelAndView.addObject("error", errorMsg);
        }
        return modelAndView;
    }
}
