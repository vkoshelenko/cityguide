package com.city.guide.web.model;

public interface CityGuideConstants {

    String DEFAULT_CITY = "defaultCity";

    String DEFAULT_STATE = "defaultState";

    String DEFAULT_COUNTRY = "defaultCountry";

    String MAX_IMAGES_COUNT = "maxImagesCount";
}
