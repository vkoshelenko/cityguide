package com.city.guide.web.util;

import com.city.guide.common.config.ApplicationPropsBean;
import com.city.guide.common.spring.ObjectFactory;
import com.city.guide.web.model.CityGuideConstants;
import com.city.guide.web.model.preferences.CityGuidePreferences;
import com.liferay.portal.kernel.util.StringPool;
import org.apache.log4j.Logger;

import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ReadOnlyException;
import javax.portlet.ValidatorException;
import java.io.IOException;

public class CityGuidePreferencesUtils {

    private static final Logger _logger = Logger.getLogger(CityGuidePreferencesUtils.class);

    private static ApplicationPropsBean props = ObjectFactory.getBean(ApplicationPropsBean.class);

    /**
     * Retrieves CityGuidePreferences from request
     *
     * @param portletRequest - PortletRequest
     * @return - CityGuidePreferences object
     */
    public static CityGuidePreferences getPreferences(PortletRequest portletRequest) {

        PortletPreferences preferences = portletRequest.getPreferences();
        CityGuidePreferences cgPreferences = new CityGuidePreferences();

        cgPreferences.setDefaultCity(preferences.getValue(CityGuideConstants.DEFAULT_CITY, props.getDefaultCity()));
        cgPreferences.setDefaultCountry(preferences.getValue(CityGuideConstants.DEFAULT_COUNTRY, props.getDefaultCountry()));
        cgPreferences.setDefaultState(preferences.getValue(CityGuideConstants.DEFAULT_STATE, props.getDefaultState()));
        int count = props.getMaxImagesCount();
        try {
            String imagesCount = preferences.getValue(CityGuideConstants.MAX_IMAGES_COUNT, String.valueOf(props.getMaxImagesCount()));
            count = Integer.parseInt(imagesCount);
        } catch (Exception e) {
            _logger.error("Can not get max images count: " + e.getMessage() + "; using default: " + count);
        }
        cgPreferences.setMaxImages(count);

        return cgPreferences;
    }

    /**
     * Stores portlet preferences
     *
     * @param portletRequest - PortletRequest
     * @param cgPreferences - CityGuidePreferences object
     */
    public static void storePreferences(PortletRequest portletRequest, CityGuidePreferences cgPreferences) throws ValidatorException, IOException, ReadOnlyException {

        PortletPreferences preferences = portletRequest.getPreferences();

        preferences.setValue(CityGuideConstants.DEFAULT_CITY, cgPreferences.getDefaultCity());
        preferences.setValue(CityGuideConstants.DEFAULT_STATE, cgPreferences.getDefaultState());
        preferences.setValue(CityGuideConstants.DEFAULT_COUNTRY, cgPreferences.getDefaultCountry());
        preferences.setValue(CityGuideConstants.MAX_IMAGES_COUNT, String.valueOf(cgPreferences.getMaxImages()));

        preferences.store();
    }

    /**
     * Returns value of portlet preference
     *
     * @param portletRequest - PortletRequest
     * @param name - preference name
     * @param defaultValue - default value of preference
     * @return - preference value
     */
    public static String getPreference(PortletRequest portletRequest, String name, String defaultValue) {
        PortletPreferences preferences = portletRequest.getPreferences();
        return preferences.getValue(name, defaultValue);
    }

    /**
     * Returns value of portlet preference
     *
     * @param portletRequest - PortletRequest
     * @param name - preference name
     * @return - preference value
     */
    public static String getPreference(PortletRequest portletRequest, String name) {
        return getPreference(portletRequest, name, StringPool.BLANK);
    }

}
