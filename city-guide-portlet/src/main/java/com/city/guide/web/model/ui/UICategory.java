package com.city.guide.web.model.ui;

import java.util.List;

public class UICategory {

    private long id;
    private String name;
    private String description;
    private List<UICategory> children;

    public UICategory() {
    }

    public UICategory(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public UICategory(long id, String name, String description, List<UICategory> children) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.children = children;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UICategory> getChildren() {
        return children;
    }

    public void setChildren(List<UICategory> children) {
        this.children = children;
    }
}
