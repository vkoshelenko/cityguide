package com.city.guide.web.model.rest;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;

public class CategoryModel {

    //not validated
    private long id;

    //not validated
    private long parentId;

    @NotNull(errorCode = "name", message = "Category name is required field.")
    @NotEmpty(errorCode = "name", message = "Category name is required field.")
    private String name;

    //not validated
    private String description;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
