package com.city.guide.web.controller;

import com.city.guide.common.model.impl.Photo;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import java.io.IOException;

@Controller
@RequestMapping("VIEW")
public class ImageDownloadController extends BaseController {

    protected Logger _logger = Logger.getLogger(getClass());

    /**
     * Provides URL for photo
     *
     * @param resourceRequest - ResourceRequest
     * @param resourceResponse - ResourceResponse
     * @param photoId - id of photo
     */
    @ResourceMapping
    public void viewImage(ResourceRequest resourceRequest,
                          ResourceResponse resourceResponse,
                          @RequestParam("photoId") Long photoId){

        Photo photo = null;
        if (photoId > 0) {
            photo = photoService.getPhoto(photoId);
        }

        if (photo != null) {
            String photoName = photo.getOriginalName();
            byte[] content = photo.getContent();
            String contentType = photo.getContentType();
            try {
                PortletResponseUtil.sendFile(resourceRequest, resourceResponse, photoName, content, contentType);
            } catch (IOException e) {
                _logger.error("Unable to send image: " + photoName);
            }
        } else {
            _logger.error("Photo not found with photoId = " + photoId);
        }
    }
}
