package com.city.guide.web.controller;

import com.city.guide.common.exception.CityGuideException;
import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.Category;
import com.city.guide.common.model.impl.Photo;
import com.city.guide.common.model.impl.SearchParams;
import com.city.guide.common.security.RoleChecker;
import com.city.guide.web.model.preferences.CityGuidePreferences;
import com.city.guide.web.util.CityGuidePreferencesUtils;
import com.city.guide.web.util.CityGuideUtils;
import com.city.guide.web.util.JacksonViewHelper;
import net.sf.oval.ConstraintViolation;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.multipart.DefaultMultipartActionRequest;
import org.springframework.web.servlet.view.AbstractView;

import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("VIEW")
public class AdController extends BaseController {

    protected Logger _logger = Logger.getLogger(getClass());

    /**
     * Obtain ModelAndView for ads list
     *
     * @return - modelAndView
     */
    @RenderMapping(params = "action=adsListView")
    public ModelAndView getAdsView(RenderRequest renderRequest){
        ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
        List<Ad> ads = adService.getAds();
        modelAndView.addObject("ads", ads);
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        modelAndView.addObject("hasPermissions", hasPermissions);
        fillModelAndView(renderRequest, modelAndView);
        return modelAndView;
    }


    /**
     * @param renderRequest - RenderRequest
     * @param categoryId - id of category
     * @return - AbstractView
     */
    @RenderMapping(params = "action=searchAds")
    public ModelAndView searchAds(RenderRequest renderRequest,
                                  @RequestParam(value = "categoryId", required = false) Long categoryId,
                                  @RequestParam(value = "name", required = false) String name) {
        ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
        SearchParams searchParams = new SearchParams(categoryId, name);
        List<Ad> ads = adService.searchAds(searchParams);
        modelAndView.addObject("ads", ads);
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("searchParams", searchParams);
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Filter ads by category
     *
     * @param renderRequest - RenderRequest
     * @param categoryId - id of category
     *
     * @return - modelAndView
     */
    @RenderMapping(params = "action=filterAds")
    public ModelAndView filterAds(RenderRequest renderRequest,
                                  @RequestParam(value = "categoryId", required = false) Long categoryId) {
        ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
        List<Ad> ads = categoryId > 0 ? adService.getCategoryAds(categoryId) : adService.getAds();
        modelAndView.addObject("ads", ads);
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Obtain ModelAndView for adding an ad
     *
     * @param renderRequest - RenderRequest
     * @param renderResponse - RenderResponse
     * @return modelAndView
     */
    @RenderMapping(params = "action=addAdView")
    public ModelAndView addAdView(RenderRequest renderRequest, RenderResponse renderResponse) {

        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
            List<Ad> ads = adService.getAds();
            modelAndView.addObject("ads", ads);
            List<Category> categories = categoryService.getCategories();
            modelAndView.addObject("info", "You have no  permissions for this action.");
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(AD_ADD_VIEW);
        //handle validation
        if (renderRequest.getAttribute("errorMap") != null) {
            Map<String,String> errorMap = (Map<String,String>)renderRequest.getAttribute("errorMap");
            modelAndView.addObject("errorMap", errorMap);
            Ad ad = (Ad)renderRequest.getAttribute("ad");
            modelAndView.addObject("ad", ad);
        }

        //add preferences object for default values
        CityGuidePreferences preferences = CityGuidePreferencesUtils.getPreferences(renderRequest);
        modelAndView.addObject("preferences", preferences);
        //add categories list
        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Obtain ModelAndView for editing the ad
     *
     * @param renderRequest - RenderRequest
     * @param renderResponse - RenderResponse
     * @return - modelAndView
     */
    @RenderMapping(params = "action=editAdView")
    public ModelAndView editAdView(RenderRequest renderRequest, RenderResponse renderResponse,
                                   @RequestParam("adId") Long adId){
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
            List<Ad> ads = adService.getAds();
            modelAndView.addObject("ads", ads);
            List<Category> categories = categoryService.getCategories();
            modelAndView.addObject("info", "You have no  permissions for this action.");
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(AD_EDIT_VIEW);
        //handle validation
        if (renderRequest.getAttribute("errorMap") != null) {
            Map<String,String> errorMap = (Map<String,String>)renderRequest.getAttribute("errorMap");
            modelAndView.addObject("errorMap", errorMap);
            Ad ad = (Ad)renderRequest.getAttribute("ad");
            modelAndView.addObject("ad", ad);
            List<Photo> photos = photoService.getAdPhotos(adId);
            modelAndView.addObject("photos", photos);
        }

        Ad ad = adService.getAd(adId);
        modelAndView.addObject("ad", ad);
        List<Photo> photos = photoService.getAdPhotos(adId);
        modelAndView.addObject("photos", photos);

        List<Category> categories = categoryService.getCategories();
        modelAndView.addObject("categories", categories);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Obtain ModelAndView for displaying ad info
     *
     * @param renderRequest - RenderRequest
     * @param adId - id of Ad
     * @return - modelAndView
     */
    @RenderMapping(params = "action=viewAdInfo")
    public ModelAndView viewAdInfo(RenderRequest renderRequest,
                                   @RequestParam(value = "adId") Long adId){
        ModelAndView modelAndView = new ModelAndView(AD_INFO_VIEW);
        Ad ad = adService.getAd(adId);
        modelAndView.addObject("ad", ad);
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        modelAndView.addObject("hasPermissions", hasPermissions);
        return modelAndView;
    }

    /**
     * Saves new ad
     *
     * @param ad - Ad
     * @param actionRequest - DefaultMultipartActionRequest
     * @param actionResponse - ActionResponse
     */
    @ActionMapping(params = "action=addAd")
    public void addAd(@ModelAttribute("ad") Ad ad,
                      DefaultMultipartActionRequest actionRequest,
                      ActionResponse actionResponse) {

        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(actionRequest);
        if (!hasPermissions) {
            actionResponse.setRenderParameter("info", "You have no  permissions for this action.");
            actionResponse.setRenderParameter("action", "adsListView");
            return;
        }

        List<ConstraintViolation> violations = ovalValidator.validate(ad);
        Map<String,String> errorMap = CityGuideUtils.getErrorMap(violations);
        Long categoryId = ad.getCategory().getId();
        if (categoryId < 0) {
            errorMap.put("category", "Category should be selected.");
        }
        if (!errorMap.isEmpty()) {
            actionResponse.setRenderParameter("action", "addAdView");
            actionRequest.setAttribute("errorMap", errorMap);
            actionRequest.setAttribute("ad", ad);
            return;
        }
        Category category = categoryService.getCategory(categoryId);
        ad.setCategory(category);
        adService.saveAd(ad);
        try {
            storePhotos(ad, actionRequest);
        } catch (CityGuideException e) {
            actionResponse.setRenderParameter("info", e.getMessage());
        }
        actionResponse.setRenderParameter("success", "Ad has been saved successfully.");
        actionResponse.setRenderParameter("action", "adsListView");
    }

    /**
     * Updates the ad
     *
     * @param ad - Ad
     * @param actionRequest - DefaultMultipartActionRequest
     * @param actionResponse - ActionResponse
     */
    @ActionMapping(params = "action=editAd")
    public void editAd(@ModelAttribute("ad") Ad ad,
                       DefaultMultipartActionRequest actionRequest,
                       ActionResponse actionResponse) {

        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(actionRequest);
        if (!hasPermissions) {
            actionResponse.setRenderParameter("info", "You have no  permissions for this action.");
            actionResponse.setRenderParameter("action", "adsListView");
            return;
        }

        List<ConstraintViolation> violations = ovalValidator.validate(ad);
        if (!violations.isEmpty()) {
            Map<String,String> errorMap = CityGuideUtils.getErrorMap(violations);
            actionResponse.setRenderParameter("action", "addAdView");
            actionRequest.setAttribute("errorMap", errorMap);
            actionRequest.setAttribute("ad", ad);
            return;
        }
        Long categoryId = ad.getCategory().getId();
        Category category = categoryService.getCategory(categoryId);
        ad.setCategory(category);
        adService.updateAd(ad);
        try {
            storePhotos(ad, actionRequest);
        } catch (CityGuideException e) {
            actionResponse.setRenderParameter("info", e.getMessage());
        }
        actionResponse.setRenderParameter("success", "Ad has been changed successfully.");
        actionResponse.setRenderParameter("action", "adsListView");
    }

    /**
     * Deletes the ad
     *
     * @param renderRequest - RenderRequest
     * @param adId - id of ad
     * @return - modelAndView
     */
    @RenderMapping(params = "action=deleteAd")
    public ModelAndView deleteAd(RenderRequest renderRequest,
                                 @RequestParam("adId") Long adId) {
        //check permissions
        boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
        if (!hasPermissions) {
            ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
            List<Ad> ads = adService.getAds();
            modelAndView.addObject("ads", ads);
            List<Category> categories = categoryService.getCategories();
            modelAndView.addObject("info", "You have no  permissions for this action.");
            modelAndView.addObject("categories", categories);
            modelAndView.addObject("hasPermissions", hasPermissions);
            return modelAndView;
        }

        ModelAndView modelAndView = new ModelAndView(ADS_LIST_VIEW);
        try {
            adService.deleteAd(adId);
            modelAndView.addObject("success", "Ad has been deleted successfully.");
            List<Ad> ads = adService.getAds();
            modelAndView.addObject("ads", ads);
        } catch (Exception e) {
            String errorMsg = "Can not delete ad: " + e.getMessage();
            _logger.error(errorMsg);
            modelAndView.addObject("error", errorMsg);
        }
        return modelAndView;
    }

    @RenderMapping(params = "action=deletePhoto")
    public
    @ResponseBody
    AbstractView deletePhoto(@RequestParam("photoId") Long photoId) {
        try {
            photoService.deletePhoto(photoId);
            return JacksonViewHelper.getJsonView("success", "true");
        } catch (Exception e) {
            _logger.error("Can not delete photo with photoId='" + photoId + "' : " + e.getMessage());
            return JacksonViewHelper.getJsonView("success", "false");
        }
    }


    /**
     * Stores photos for ad from actionRequest
     *
     * @param ad - Ad
     * @param actionRequest - DefaultMultipartActionRequest
     */
    private void storePhotos(Ad ad, DefaultMultipartActionRequest actionRequest) throws CityGuideException {
        List<MultipartFile> files = actionRequest.getFiles("photo");
        CityGuidePreferences preferences = CityGuidePreferencesUtils.getPreferences(actionRequest);
        int maxPhotos = preferences.getMaxImages();
        long photosCount = photoService.getAdPhotosCount(ad.getId());
        if (files != null && files.size() > 0) {
            for (MultipartFile file : files) {
                byte[] content = new byte[]{};
                try {
                    content = file.getBytes();
                } catch (IOException e) {
                    _logger.error("Can not get image content: " + file.getOriginalFilename());
                }
                String originalName = file.getOriginalFilename();
                if (content != null && content.length > 0 && StringUtils.isNotBlank(originalName)) {
                    //check max count of photos
                    if (photosCount >= maxPhotos) {
                        throw new CityGuideException("Not all photos have been saved. Maximal count of photos allowed is " + maxPhotos);
                    }
                    Photo photo = new Photo();
                    String contentType = file.getContentType();
                    photo.setAd(ad);
                    photo.setContent(content);
                    photo.setContentType(contentType);
                    photo.setOriginalName(originalName);
                    photoService.savePhoto(photo);
                    photosCount++;
                }
            }
        }
    }

    /**
     * Fills ModelAndView with error/info/success messages
     *
     * @param renderRequest - RenderRequest
     * @param modelAndView - ModelAndView
     */
    private void fillModelAndView(RenderRequest renderRequest, ModelAndView modelAndView) {
        if (renderRequest.getParameter("error") != null) {
            modelAndView.addObject("error", renderRequest.getParameter("error"));
        }
        if (renderRequest.getParameter("info") != null) {
            modelAndView.addObject("info", renderRequest.getParameter("info"));
        }
        if (renderRequest.getParameter("success") != null) {
            modelAndView.addObject("success", renderRequest.getParameter("success"));
        }
    }
}
