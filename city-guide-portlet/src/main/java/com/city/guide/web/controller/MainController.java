package com.city.guide.web.controller;

import com.city.guide.common.model.impl.Ad;
import com.city.guide.common.model.impl.Category;
import com.city.guide.common.model.impl.Photo;
import com.city.guide.common.security.RoleChecker;
import com.city.guide.web.util.FriendlyURLConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;

import javax.portlet.RenderRequest;
import java.util.List;

@Controller
@RequestMapping("VIEW")
public class MainController extends BaseController {

    /**
     * Empty action mapping
     */
    @ActionMapping
    public void action(){
    }

    /**
     * Friendly URL handler
     *
     * @param adId - FriendlyURLConstants.AD_PARAMETER parameter from friendlyURL
     * @param categoryId - FriendlyURLConstants.CATEGORY_PARAMETER parameter from friendlyURL
     * @return modelAndView
     */
    @RequestMapping
    public ModelAndView view(RenderRequest renderRequest,
                             @RequestParam(value = FriendlyURLConstants.AD_PARAMETER, required = false) Long adId,
                             @RequestParam(value = FriendlyURLConstants.CATEGORY_PARAMETER, required = false) Long categoryId) {

        //check ad friendlyUrl
        if (adId != null) {
            Ad ad = adService.getAd(adId);
            ModelAndView modelAndView = new ModelAndView(AD_INFO_VIEW);
            modelAndView.addObject("ad", ad);
            List<Photo> photos = photoService.getAdPhotos(adId);
            modelAndView.addObject("photos", photos);
            //check permissions
            boolean hasPermissions = RoleChecker.hasAdsManagementPermissions(renderRequest);
            modelAndView.addObject("hasPermissions", hasPermissions);
            return modelAndView;
        }

        //check category friendlyUrl
        if (categoryId != null) {
            ModelAndView modelAndView = new ModelAndView(CATEGORY_INFO_VIEW);
            Category category = categoryService.getCategory(categoryId);
            modelAndView.addObject("category", category);

            List<Ad> categoryAds = adService.getCategoryAds(categoryId);
            modelAndView.addObject("categoryAds", categoryAds);

            //check permissions
            boolean hasPermissions = RoleChecker.hasCategoryManagementPermissions(renderRequest);
            modelAndView.addObject("hasPermissions", hasPermissions);
            return modelAndView;
        }

        return new ModelAndView(DEFAULT_VIEW);
    }
}
