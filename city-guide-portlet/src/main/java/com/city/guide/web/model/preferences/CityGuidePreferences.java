package com.city.guide.web.model.preferences;

import net.sf.oval.constraint.NotEmpty;
import net.sf.oval.constraint.NotNull;
import net.sf.oval.constraint.Range;

public class CityGuidePreferences {

    @NotNull(errorCode = "defaultCity", message = "Please, specify default city name.")
    @NotEmpty(errorCode = "defaultCity", message = "Please, specify default city name.")
    private String defaultCity;

    @NotNull(errorCode = "defaultState", message = "Please, specify default state name.")
    @NotEmpty(errorCode = "defaultState", message = "Please, specify default state name.")
    private String defaultState;

    @NotNull(errorCode = "defaultCountry", message = "Please, specify default country name.")
    @NotEmpty(errorCode = "defaultCountry", message = "Please, specify default country name.")
    private String defaultCountry;

    @NotNull(errorCode = "maxImages", message = "Please, specify default country name.")
    @NotEmpty(errorCode = "maxImages", message = "Please, specify default country name.")
    @Range(min = 1, max = 100, errorCode = "maxImages", message = "maxImages count must be between 1 and 100")
    private Integer maxImages;

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    public String getDefaultCity() {
        return defaultCity;
    }

    public void setDefaultCity(String defaultCity) {
        this.defaultCity = defaultCity;
    }

    public String getDefaultState() {
        return defaultState;
    }

    public void setDefaultState(String defaultState) {
        this.defaultState = defaultState;
    }

    public Integer getMaxImages() {
        return maxImages;
    }

    public void setMaxImages(Integer maxImages) {
        this.maxImages = maxImages;
    }
}
