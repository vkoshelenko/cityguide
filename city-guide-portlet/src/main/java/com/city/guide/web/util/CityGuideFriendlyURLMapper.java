package com.city.guide.web.util;

import com.liferay.portal.kernel.portlet.BaseFriendlyURLMapper;
import com.liferay.portal.kernel.portlet.LiferayPortletURL;
import com.liferay.portal.kernel.util.StringPool;

import java.util.Map;

public class CityGuideFriendlyURLMapper extends BaseFriendlyURLMapper {

    @Override
    public String buildPath(LiferayPortletURL liferayPortletURL) {
        return StringPool.BLANK;
    }

    /**
     *
     * URL example:
     * http://localhost:8080/web/guest/city-guide/-/category/2/pub
     * http://localhost:8080/web/guest/city-guide/-/ad/19/irish-pub
     *
     * @param friendlyURLPath - friendlyURLPath
     * @param params - params
     * @param stringObjectMap -stringObjectMap
     */
    @Override
    public void populateParams(String friendlyURLPath, Map<String, String[]> params, Map<String, Object> stringObjectMap) {

        if (friendlyURLPath.startsWith(FriendlyURLConstants.AD_URL_START)) {

            String[] parts = friendlyURLPath.split(StringPool.SLASH);

            String adId = null;
            Integer index = FriendlyURLConstants.AD_PARAMETER_INDEX;
            if (parts.length > index) {
                adId = parts[index];
            }

            addParamToUrl(params, "p_p_id", _PORTLET_ID);
            addParamToUrl(params, FriendlyURLConstants.AD_PARAMETER, adId);

        } else if (friendlyURLPath.startsWith(FriendlyURLConstants.CATEGORY_URL_START)) {

            String[] parts = friendlyURLPath.split(StringPool.SLASH);

            String categoryId = null;
            Integer index = FriendlyURLConstants.CATEGORY_PARAMETER_INDEX;
            if (parts.length > index) {
                categoryId = parts[index];
            }

            addParamToUrl(params, "p_p_id", _PORTLET_ID);
            addParamToUrl(params, FriendlyURLConstants.CATEGORY_PARAMETER, categoryId);
        }
    }

    private void addParamToUrl(Map<String, String[]> params, String name, String value) {
        params.put(name, new String[] {value});
    }

    @Override
    public String getMapping() {
        return _MAPPING;
    }

    @Override
    public String getPortletId() {
        return  _PORTLET_ID;
    }

    public static final String _PORTLET_ID = "cityguide_WAR_cityguide";
    public static final String _MAPPING = "city-guide";
}
