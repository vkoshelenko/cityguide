package com.city.guide.web.controller;

import com.city.guide.web.model.preferences.CityGuidePreferences;
import com.city.guide.web.util.CityGuidePreferencesUtils;
import com.city.guide.web.util.CityGuideUtils;
import net.sf.oval.ConstraintViolation;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.RenderRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("EDIT")
public class EditController extends BaseController {

    private static final Logger _logger = Logger.getLogger(EditController.class);

    @RequestMapping
    public ModelAndView view(RenderRequest renderRequest){

        ModelMap modelMap = new ModelMap();

        CityGuidePreferences preferences = CityGuidePreferencesUtils.getPreferences(renderRequest);

        modelMap.put("preferences", preferences);

        return new ModelAndView(EDIT_MODE_VIEW, modelMap);
    }

    @RenderMapping(params = "action=savePreferences")
    public ModelAndView save(@ModelAttribute("cityGuidePreferences") CityGuidePreferences cityGuidePreferences,
                             RenderRequest renderRequest) {

        ModelMap modelMap = new ModelMap();
        modelMap.put("preferences", cityGuidePreferences);

        List<ConstraintViolation> violations = ovalValidator.validate(cityGuidePreferences);
        if (!violations.isEmpty()) {
            Map<String,String> errorMap = CityGuideUtils.getErrorMap(violations);
            renderRequest.setAttribute("errorMap", errorMap);
        } else {
            try {
                CityGuidePreferencesUtils.storePreferences(renderRequest, cityGuidePreferences);
                modelMap.put("success", "Preferences have been saved successfully.");
            } catch (Exception e) {
                String errorMsg = "Can not store preferences: " + e.getMessage();
                _logger.error(errorMsg);
                modelMap.put("error", errorMsg);
            }
        }

        return new ModelAndView(EDIT_MODE_VIEW, modelMap);
    }

}
