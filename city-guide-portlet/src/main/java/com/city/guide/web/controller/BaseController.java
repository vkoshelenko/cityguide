package com.city.guide.web.controller;

import com.city.guide.common.config.ApplicationPropsBean;
import com.city.guide.common.service.AdService;
import com.city.guide.common.service.CategoryService;
import com.city.guide.common.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

    @Autowired
    protected AdService adService;

    @Autowired
    protected CategoryService categoryService;

    @Autowired
    protected PhotoService photoService;

    @Autowired
    protected net.sf.oval.Validator ovalValidator;

    @Autowired
    protected ApplicationPropsBean props;

    protected static final String DEFAULT_SUCCESS_RESULT = "{\"success\":true}";

    protected static final String DEFAULT_ERROR_RESULT = "{\"success\":false}";


    protected static final String DEFAULT_VIEW = "/view/view";

    protected static final String AD_INFO_VIEW = "/view/ad/ad-info";
    protected static final String ADS_LIST_VIEW = "/view/ad/ads-list";
    protected static final String AD_ADD_VIEW = "/view/ad/add-ad";
    protected static final String AD_EDIT_VIEW = "/view/ad/edit-ad";

    protected static final String CATEGORY_INFO_VIEW = "/view/category/category-info";
    protected static final String CATEGORIES_LIST_VIEW = "/view/category/categories-list";
    protected static final String CATEGORY_ADD_VIEW = "/view/category/add-category";
    protected static final String CATEGORY_EDIT_VIEW = "/view/category/edit-category";

    protected static final String EDIT_MODE_VIEW = "/edit/view";



}
