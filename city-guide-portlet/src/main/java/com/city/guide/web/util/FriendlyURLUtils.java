package com.city.guide.web.util;

import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.Portal;
import com.liferay.portal.util.PortalUtil;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

public class FriendlyURLUtils {

    public static String buildBaseAdFriendlyUrl(PortletRequest portletRequest){
        HttpServletRequest request = PortalUtil.getHttpServletRequest(portletRequest);
        String currentCompleteURL = PortalUtil.getCurrentCompleteURL(request);
        String mainUrl = currentCompleteURL;
        if (currentCompleteURL.contains(Portal.FRIENDLY_URL_SEPARATOR)) {
            mainUrl =  currentCompleteURL.substring(0, currentCompleteURL.indexOf(Portal.FRIENDLY_URL_SEPARATOR));
        } else if (currentCompleteURL.contains(StringPool.QUESTION)) {
            mainUrl =  currentCompleteURL.substring(0, currentCompleteURL.indexOf(StringPool.QUESTION));
        }
        StringBuilder url = new StringBuilder(mainUrl);
        url.append(Portal.FRIENDLY_URL_SEPARATOR);
        url.append(FriendlyURLConstants.AD_NAME);
        return url.toString();
    }

    public static String buildBaseCategoryFriendlyUrl(PortletRequest portletRequest){
        HttpServletRequest request = PortalUtil.getHttpServletRequest(portletRequest);
        String currentCompleteURL = PortalUtil.getCurrentCompleteURL(request);
        String mainUrl = currentCompleteURL;
        if (currentCompleteURL.contains(Portal.FRIENDLY_URL_SEPARATOR)) {
            mainUrl =  currentCompleteURL.substring(0, currentCompleteURL.indexOf(Portal.FRIENDLY_URL_SEPARATOR));
        } else if (currentCompleteURL.contains(StringPool.QUESTION)) {
            mainUrl =  currentCompleteURL.substring(0, currentCompleteURL.indexOf(StringPool.QUESTION));
        }
        StringBuilder url = new StringBuilder(mainUrl);
        url.append(Portal.FRIENDLY_URL_SEPARATOR);
        url.append(FriendlyURLConstants.CATEGORY_NAME);
        return url.toString();
    }

}
